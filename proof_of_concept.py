import time

from pygame import mixer  # Load the popular external library

#mixer.music.load('./wird_gebaut.mp3')

# get_device_info(an_id) -> (interf, name, input, output, opened)
import pygame.midi

# https://pythonhosted.org/pynput/keyboard.html
from pynput.keyboard import Key, Controller

def LedGetColor( red, green ):
    led = 0
    red = min( int(red), 3 ) # make int and limit to <=3
    red = max( red, 0 )      # no negative numbers
    green = min( int(green), 3 ) # make int and limit to <=3
    green = max( green, 0 )      # no negative numbers
    led |= red
    led |= green << 4 
    return led

# input_output integer 2 for input device or 3 for output_device 
def get_input_output_id(input_output):
    launchpad_id = 0
    for x in range(0,pygame.midi.get_count()):
        print(pygame.midi.get_device_info(x))
        # if b'Launchpad Mini MK3 MIDI 1' in pygame.midi.get_device_info(x)[1] and pygame.midi.get_device_info(x)[input_output] == 1:
        if b'Launchpad' in pygame.midi.get_device_info(x)[1] and pygame.midi.get_device_info(x)[input_output] == 1:
            launchpad_id = x
            print(input_output)
            print(pygame.midi.get_device_info(x)[input_output])
            #print(pygame.midi.get_device_info(x)[1])
            # print("Launchpad")
            
            #print(x)
        
    return launchpad_id

def main():
    mixer.init()
    pygame.midi.init()
    keyboard = Controller()
    input_id = get_input_output_id(2)
    output_id = get_input_output_id(3)


    i = pygame.midi.Input(input_id)

    o = pygame.midi.Output(output_id, 1)
    print("starting")
    going = True

    alt_toggle = False
    ctrl_toggle = False
    ctrl_timestamp = 0

    while going:
        if alt_toggle and keyboard.pressed(Key.alt):
            keyboard.press(Key.alt)
        if ctrl_toggle and keyboard.pressed(Key.ctrl):
            keyboard.press(Key.ctrl)
        if i.poll():
            midi_events = i.read(10)
            yield midi_events
            if midi_events[0][0][0] == 176:
                if midi_events[0][0][2] > 0:
                    if midi_events[0][0][1] == 104:
                        alt_toggle = True
                        o.write_short( 176  , 104, LedGetColor(1,0))

                else:
                    if midi_events[0][0][1] == 104:
                        alt_toggle = False
                        keyboard.release(Key.alt)
                        o.write_short( 176  , 104, LedGetColor(0,0))

                # print("176 note: " + str(midi_events[0][1]))
            elif midi_events[0][0][0] == 144:
                # print("144 note: " + str(midi_events[0][1]))
                pass

            if midi_events[0][0][0] == 176:
                if midi_events[0][0][2] > 0:
                    if midi_events[0][0][1] == 105 and ctrl_timestamp == 0:
                        ctrl_toggle = True
                        ctrl_timestamp = midi_events[0][1]
                        # print("ctrl_timestamp: " + str(ctrl_timestamp))
                        o.write_short( 176  , 105, LedGetColor(1,0))

                else:
                    if midi_events[0][0][1] == 105 and midi_events[0][1] - ctrl_timestamp >= 500:
                        ctrl_timestamp = 0
                        ctrl_toggle = False
                        keyboard.release(Key.ctrl)
                        o.write_short( 176  , 105, LedGetColor(0,0))

                # print("176 note: " + str(midi_events))
            elif midi_events[0][0][0] == 144:
                pass
                # print("144 note: " + str(midi_events))

if __name__ == '__main__':
    main()